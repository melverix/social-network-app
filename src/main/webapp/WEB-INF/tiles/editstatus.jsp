<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="ISO-8859-2"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class=row>
	<div class="col-md-8 col-md-offset-2">

		<div class="panel panel-info">
			<div class="panel-heading">Editing a status update</div>

			<form:form modelAttribute="statusUpdate">
				<form:input type="hidden" path="id" />
				<form:input type="hidden" path="added" />

				<div class="errors">
					<form:errors path="text" />
				</div>
				<div class="form-group">
					<form:textarea path="text" name="text" rows="10" cols="50"></form:textarea>
				</div>
				<input type="submit" name="submit" value="Save status" />
			</form:form>
		</div>

		<script
			src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>

		<script>
			tinymce.init({
				selector : 'textarea',
				plugins : 'link'
			});
		</script>
	</div>
</div>
