package com.webapp.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.webapp.example.model.StatusUpdate;
import com.webapp.example.service.StatusUpdateService;

@Controller
public class PageController {

	@Autowired
	private StatusUpdateService statusUpdateService;

	@RequestMapping("/")
	ModelAndView home(ModelAndView modelAndView) {
		StatusUpdate latestStatus = statusUpdateService.getLatest();
		modelAndView.getModel().put("latestStatus", latestStatus);
		modelAndView.setViewName("app.homepage");
		return modelAndView;
	}

	@RequestMapping("/about")
	String about() {
		return "app.about";
	}

}
