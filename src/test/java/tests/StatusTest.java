package tests;

import  static org.junit.Assert.assertEquals;
import  static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.webapp.example.App;
import com.webapp.example.model.StatusUpdate;
import com.webapp.example.model.StatusUpdateDao;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=App.class)
@WebAppConfiguration
@Transactional
public class StatusTest {

@Autowired	
private StatusUpdateDao statusRepository;
	
	@Test
	public void saveTest() {
	StatusUpdate status = new StatusUpdate("This is a test status update");
	statusRepository.save(status);
	assertNotNull("Id should not be null", status.getId());
	assertNotNull("Date should not be null", status.getAdded());
	
	Optional<StatusUpdate> retreived = statusRepository.findById(status.getId());
	
	assertEquals(true, status.equals(retreived.get()));
	System.out.println(retreived.get().getAdded());
	}
	
	@Test
	public void testFindLatest() {
		
		Calendar calendar = Calendar.getInstance();
		StatusUpdate lastStatusUpdate = null;
		
		for (int i = 0; i < 10; i++) {
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			StatusUpdate status = new StatusUpdate("Status update " + i, calendar.getTime());
			statusRepository.save(status);
			lastStatusUpdate = status;
		}
		StatusUpdate retreived = statusRepository.findFirstByOrderByAddedDesc();
		assertEquals("Last status update and retreived status are equal", lastStatusUpdate, retreived);
	}
}
