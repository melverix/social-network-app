<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-2"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url var="url" value="/viewstatus" />

<div class=row>
	<div class="col-md-8 col-md-offset-2">

		<c:if test="${page.totalPages!=1}">
			<div class="pagination">
				<c:forEach var="pageNumber" begin="1" end="${page.totalPages}">
					<c:choose>
						<c:when test="${page.number != pageNumber - 1}">
							<a href="${url}?p=<c:out value = "${pageNumber}"/>"><c:out
									value="${pageNumber}"></c:out></a>
						</c:when>

						<c:otherwise>
							<strong><c:out value="${pageNumber}"></c:out></strong>
						</c:otherwise>
					</c:choose>

					<c:if test="${pageNumber != page.totalPages}">|</c:if>
				</c:forEach>
			</div>
		</c:if>
		<c:forEach var="statusUpdate" items="${page.content}">
			<c:url var="editLink" value="/editstatus?id=${statusUpdate.id}"></c:url>
			<c:url var="deleteLink" value="/deletestatus?id=${statusUpdate.id}"></c:url>

			<div class="panel panel-info">
				<div class="panel-heading">
					Status added on:
					<fmt:formatDate pattern="dd'.'MM'.'y 'at' hh:mm:ss"
						value="${statusUpdate.added}" />
				</div>
				<div class="panel-body">

					<div>${statusUpdate.text}</div>
					<div class="edit-links pull-right">
						<a href="${editLink}">edit</a> | <a onclick = "return confirm('Do you want to delete this entry?')" href="${deleteLink}">delete</a>
					</div>
				</div>
			</div>

		</c:forEach>
	</div>
</div>

